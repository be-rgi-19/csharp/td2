﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TD2
{
    class Voiture
    {
        private string marque;
        private string type;
        private int puissance;
        private int kilometrage;
        private int reservoire;
        private double carburant;
        private double consoMoyenne;

        public Voiture(string marque, string type, int puissance, int reservoire, double consoMoyenne)
        {
            this.marque = marque;
            this.type = type;
            this.puissance = puissance;
            this.reservoire = reservoire;
            this.consoMoyenne = consoMoyenne;
            this.kilometrage = 0;
            this.carburant = 0;
        }

        public void Klaxonne()
        {
            Console.WriteLine("{0}: Pouet", marque);
        }

        public void Klaxonne(int nb)
        {
            for (int i = 0; i < nb; i++)
            {
                Console.Write("Pouet");
            }
        }

        public void Roule(int nbKm)
        {
            double KmMax = carburant * 100 / consoMoyenne;
            if (nbKm> KmMax)
            {
                Console.WriteLine("{0}: Trajet impossible, Panne sêche dans {1}", marque, KmMax);
            }
            else
            {
                double qteConsommee = nbKm * consoMoyenne / 100;
                Console.WriteLine("{0}- carburant consommé:{1}",marque, qteConsommee);
                kilometrage = kilometrage + nbKm;
                Console.WriteLine("Nouveau kilometrage:{0}", kilometrage);
                carburant = carburant - qteConsommee;
                Console.WriteLine("Il reste dans le réservoir {0}",carburant);
            }
        }

        public void AjouterCArburant(double nbLitres)
        {
            if (nbLitres < 0)
                Console.WriteLine("{0}: litrage négatif", marque);
            else if (carburant + nbLitres > reservoire)
                Console.WriteLine("{0}: ajout impossible", marque);
            else
            {
                carburant = carburant + nbLitres;
                Console.WriteLine("{0}: quantité dans le réservoir {1}", marque, carburant);
            }}
    }
}
